//
//  ASRequestModal.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.
//

import Foundation
import UIKit

public enum MediaType: Int {
    case image
    case video
    case audio
}

public class ASRequestModal: NSObject {
    
    public var inputParameters: Dictionary<String,Any>?;
    public var urlParameters: [String : String]?;
    public var requestType = RequestType.POST;
    public var images:Dictionary<String,Data>?


    public var baseUrl:String?
    public var urlIdentifier:String?
    public var operationUrlString:String?
    public var spinnerText:String?
    public var modalClassName:String?
    public var requestHeader:[String:String]?
    public var cachesData : Bool =  false
    public var stubEnabled: Bool = false
    public var consumeError : Bool = false
    public var customErrorHandler = false
    public var action : UIAlertAction?
    public var email: String = ""
    public var isFormPost: Bool = false
    public var isMultiPart: Bool = false
    public var token: String = ""
    public var isUserLoggedIn: Bool = false
    public var codableData: Data?
    public var mediaType: MediaType = .image
    public var fileName: String?
    
    override public init() {
        self.inputParameters = nil;
        self.urlParameters = nil;
        self.urlIdentifier = nil;
        self.baseUrl = nil;
        self.operationUrlString = nil;
        self.spinnerText = nil;
        self.modalClassName = nil;
        self.requestHeader = nil;
        self.codableData = nil
    }
    
    public init(requestIdentifier: String, iParams: Dictionary<String, Any>?, uParams: Dictionary<String, String>?, requestHeader:[String:String]? = nil, cData: Data? = nil) {
        
        self.inputParameters = iParams;
        self.urlParameters = uParams;
        self.urlIdentifier = requestIdentifier;
        self.baseUrl = nil;
        self.operationUrlString = nil;
        self.requestHeader = requestHeader
        self.spinnerText = nil;
        self.modalClassName = nil;
        self.codableData = cData
    }
    
    public func userLoggedIn() {
        self.isUserLoggedIn = true
    }
    
    public func fileNameForUpload(fileName: String) {
        self.fileName = fileName
    }
    
    public func fetchToken() -> String? {
        let userDefaults = UserDefaults.standard
        if let token = userDefaults.value(forKey: "token") as? String {
            return token
        }
        return nil
    }
}
