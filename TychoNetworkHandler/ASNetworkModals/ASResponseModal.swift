//
//  ASResponseModal.swift
//  PUGs
//
//  Created by Anshumaan on 06/05/20.
//  Copyright © 2020 Anshumaan. All rights reserved.
//

import Foundation


public struct ASNetObject {
    
    public let responseDict:AnyObject?
    public let error:NSError?
    public let statusCode:Int?
    public var isSuccess:Bool = false
    public let operationSuccessful:Bool?
    public let serverMessage : String?
    public var responseString : String?
    public var responseData: Data?
    
    public init(response:Data?, err:NSError? , status:Int?, success:Bool, opSuccess:Bool?, message:String?){
        
        self.statusCode = status;
        self.isSuccess  = success;
        self.error = err;
        self.responseData = response;
        self.operationSuccessful = opSuccess;
        self.serverMessage = message;
        self.responseDict = nil
    }
}

