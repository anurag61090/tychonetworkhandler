//
//  ParsableProtocol.swift
//  PUGs
//
//  Created by Anshumaan on 06/05/20.
//  Copyright © 2020 Anshumaan. All rights reserved.
//

import Foundation

protocol ParsableProtocol{
    // apply to structures that can be parsed through a network response
   // static func  populateWithResponseDictionart(responseDict :Dictionary<String, String>)->Any;
    //func populateWithResponseDictionart(responseDict :Dictionary<String, String>);
    
    init(responseDict :Dictionary<String, AnyObject>);
  //  func getDataDictionary()->NSDictionary;
    
    
    
    //init(responseDict : NSDictionary);
}
