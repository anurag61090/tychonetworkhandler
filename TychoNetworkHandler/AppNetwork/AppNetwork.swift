//
//  AppNetwork.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.

import Foundation
import UIKit
import Alamofire

public enum APIError<T>: Error {
    case networkError
    case apiError(statusCode: Int, error: Error?)
    case dataNotFound
    case invalidURL
    case parsingError
    case responseError(statusCode: Int, model: T)
}

public class AppNetwork: NSObject {
    
    public var requestConstructor : ASRequestConstructor?
    public var timer : Timer?
    public var alertShown : Bool = false
    
    public init(filePath: String) {
        super.init()
        let networkFilePath = Bundle.main.path(forResource: filePath, ofType:"plist");
        requestConstructor = ASRequestConstructor(filePath: networkFilePath!);
    }
    
    public func cancelNetworkRequests(){
        let session = AF
        session.session.getAllTasks { (tasks : [URLSessionTask]) in
            for task in tasks{
                task.cancel()
            }
        }
    }
    
    public func getAPIResponseFor<T: Decodable>(request: ASRequestModal, callback: @escaping ResponseClosure<T>) {
        if AppReachability.isNetworkReachable == true {
            requestConstructor?.processUpdatedRequestFor(request: request, callback: callback)
            closeTimer()
        }else{
            callback(.failure(APIError.networkError))
        }
    }
    
    public func closeTimer(){
        if(self.timer != nil){
            timer?.invalidate()
        }
    }
    
    public func startTimer(){
        closeTimer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(AppNetwork.updateAlertStatus), userInfo: nil, repeats: false)
    }
    
    @objc public func updateAlertStatus(){
        self.alertShown = false
    }
    
    public func showAlertAppDelegate(title : String,message : String,buttonTitle : String,window: UIWindow){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
}

func printNetworkLogs(response: AFDataResponse<Data?>) {
    print("************************ Request Start**************************");
    print(response.request ?? "")  // original URL request
    print("************************ Request End **************************");
    
    print("************************ Response Data **************************");
    let data = response.data
    var datastring = "";
    if(data != nil){
        datastring = String(data: data!, encoding: String.Encoding.utf8)!
        print(datastring)
    }else{
        print("\n\n valid response not found");
    }
    
    print("************************ Response Results **************************");
    //print(response.result)   // result of response serialization
    print("************************ Response Results END **************************");
    
    print("test");
}

