//
//  AppReachability.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class AppReachability: NSObject {

    static let sharedInstance =  AppReachability();
    let manager = NetworkReachabilityManager(host: "www.google.com")
    var isReachable : Bool = true
    static var  isNetworkReachable:Bool = true
    var sendUpdateToHome:Bool =  true
    var homePageListener: UIViewController?

    override init() {
        super.init()
        startNetworkReachabilityObserver()
        self.isReachable = self.manager!.isReachable
        print("network reachable \(self.manager!.isReachable)")
        AppReachability.isNetworkReachable = self.isReachable
    }
    
    private func startNetworkReachabilityObserver() {
        
        manager?.startListening(onUpdatePerforming: { (status) in
            
            switch status {
            case .notReachable:
                print("not reachable")
                
            case .unknown:
                print("unknown")
                
            case .reachable(.cellular):
                print("WWAN")
                
            case .reachable(.ethernetOrWiFi):
                print("WWAN")
            }
            print("Network Status Changed: \(status)")
            print("network reachable \(self.manager!.isReachable)")
            self.isReachable = self.manager!.isReachable
            AppReachability.isNetworkReachable = self.isReachable
            
            if((self.isReachable == true) && (self.homePageListener != nil)){
//                self.homePageListener?.networkReachabilityChanged(isReachable: self.isReachable)
            }
        })
    }
    
    func subscribeToNetworkUpdate(homePage: UIViewController){
        self.homePageListener = homePage
    }
    
    func removeHomePageListener(){
        if(self.homePageListener != nil){
            self.homePageListener = nil
        }
        
    }

}


