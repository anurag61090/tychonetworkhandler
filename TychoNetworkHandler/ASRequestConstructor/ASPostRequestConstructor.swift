//
//  ASPostRequestConstructor.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.
//

import Foundation
import Alamofire

struct ASPOSTRequestConstructor {
    
    static func getResponseForRequest<T: Decodable>(request: ASRequestModal, callback: @escaping (ResponseClosure<T>)){
        let url =  request.baseUrl! + request.operationUrlString! ;
        let header = HTTPHeaders(request.requestHeader!)
        AF.request(url, method: .post, parameters: request.inputParameters, encoding: JSONEncoding.default, headers: header).response { (response) in

            #if DEBUG
            printNetworkLogs(response: response)
            #endif
            
            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                if let responseData = response.data {
                    if let model = try? decoder.decode(T.self, from: responseData) {
                        if (200...299).contains(response.response?.statusCode ?? 200) {
                            callback(.success(model))
                        } else {
                            callback(.failure(.responseError(statusCode: response.response?.statusCode ?? 401, model: model)))
                        }
                    } else {
                        callback(.failure(.parsingError))
                    }
                } else {
                    callback(.failure(.dataNotFound))
                }
            case .failure:
                callback(.failure(.apiError(statusCode: response.response?.statusCode ?? 401, error: response.error)))
            }
        }
    }
    
    
    static func getResponseForMultipartRequest<T: Decodable>(request: ASRequestModal, callback: @escaping (ResponseClosure<T>)) {
        let url =  request.baseUrl! + request.operationUrlString! ;
        let headers: HTTPHeaders = HTTPHeaders(request.requestHeader!)

        AF.upload(multipartFormData: { (formData) in
            if request.inputParameters != nil{
                for (key,value) in request.inputParameters!{
                        if value is Data{
                            let filename = request.fileName
                            var mimeType = "image/png"
                            if request.mediaType == .video {
                                mimeType = "video/mp4"
                            } else if request.mediaType == .audio {
                                mimeType = "audio/mp4"
                            }
                            formData.append((value as! Data), withName: key, fileName: filename, mimeType: mimeType)
                        }else {
                            if let valueData = (value as? String)?.data(using: String.Encoding.utf8){
                                formData.append(valueData, withName: key)
                            }
                        }
                    }
                }
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers, interceptor: nil, fileManager: FileManager.default, requestModifier: { (request) in
            request.timeoutInterval = 7200
            
        }).response { (response) in
            
            #if DEBUG
            printNetworkLogs(response: response)
            #endif
            
            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                if let responseData = response.data {
                    if let model = try? decoder.decode(T.self, from: responseData) {
                        if (200...299).contains(response.response?.statusCode ?? 200) {
                            callback(.success(model))
                        } else {
                            callback(.failure(.responseError(statusCode: response.response?.statusCode ?? 401, model: model)))
                        }
                    } else {
                        callback(.failure(.parsingError))
                    }
                } else {
                    callback(.failure(.dataNotFound))
                }
            case .failure:
                callback(.failure(.apiError(statusCode: response.response?.statusCode ?? 401, error: response.error)))
            }
        }
    }            
}
    
