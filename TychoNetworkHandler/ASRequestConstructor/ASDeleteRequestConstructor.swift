//
//  ASDeleteRequestConstructor.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.
//

import Foundation
import Alamofire


struct ASDELETERequestConstructor {
    
    static func getResponseForRequest<T: Decodable>(request: ASRequestModal, callback: @escaping (ResponseClosure<T>)){
        let url =  request.baseUrl! + request.operationUrlString! ;
        let header = HTTPHeaders(request.requestHeader!)

        AF.request(url, method: .delete, parameters: request.inputParameters, encoding: JSONEncoding.default, headers: header, interceptor: nil, requestModifier: nil).response { response in
            #if DEBUG
            printNetworkLogs(response: response)
            #endif

            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                if let responseData = response.data {
                    if let model = try? decoder.decode(T.self, from: responseData) {
                        if (200...299).contains(response.response?.statusCode ?? 200) {
                            callback(.success(model))
                        } else {
                            callback(.failure(.responseError(statusCode: response.response?.statusCode ?? 401, model: model)))
                        }
                    } else {
                        callback(.failure(.parsingError))
                    }
                } else {
                    callback(.failure(.dataNotFound))
                }
            case .failure:
                callback(.failure(.apiError(statusCode: response.response?.statusCode ?? 401, error: response.error)))
            }
        }
    }
}
