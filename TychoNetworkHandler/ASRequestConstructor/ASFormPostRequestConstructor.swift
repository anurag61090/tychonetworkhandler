//
//  ASFormPostRequestConstructor.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.
//

import UIKit
import Alamofire

class ASFormPostRequestConstructor: NSObject {

    class func getResponseForRequest<T: Decodable>(_ request: ASRequestModal, callback: @escaping ResponseClosure<T>){
        
        let url =  request.baseUrl! + request.operationUrlString! ;
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded", "Platform":"iOS"]
        let header = HTTPHeaders(headers)
        
        AF.request(url, method: .post, parameters: request.inputParameters, encoding: URLEncoding.default, headers: header, interceptor: .none, requestModifier: .none).response { (response) in
            
            #if DEBUG
            printNetworkLogs(response: response)
            #endif
            
            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                if let responseData = response.data {
                    if let model = try? decoder.decode(T.self, from: responseData) {
                        if (200...299).contains(response.response?.statusCode ?? 200) {
                            callback(.success(model))
                        } else {
                            callback(.failure(.responseError(statusCode: response.response?.statusCode ?? 401, model: model)))
                        }
                    } else {
                        callback(.failure(.parsingError))
                    }
                } else {
                    callback(.failure(.dataNotFound))
                }
            case .failure:
                callback(.failure(.apiError(statusCode: response.response?.statusCode ?? 401, error: response.error)))
            }
        }
    }
    
    
//MARK:- MULTI-PART IMAGE
    
    class func getResponseForMultipartRequest<T: Decodable>(_ request: ASRequestModal, callback: @escaping ResponseClosure<T>){
        
        let url =  request.baseUrl! + request.operationUrlString! ;
        let headers: HTTPHeaders = HTTPHeaders(request.requestHeader!)
        AF.upload(multipartFormData: { (multipartFormData) in
            if(request.images != nil){
                for (key, value) in request.images!{
                    multipartFormData.append(value, withName: key, fileName: "image.png", mimeType: "image/png")
                }
            }
            if(request.inputParameters != nil){
                for (key,value) in request.inputParameters!{
                    let valStr = value as! String
                    multipartFormData.append(valStr.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        }, to: url,
            method:.post,
            headers:headers).response { (response) in

            #if DEBUG
            printNetworkLogs(response: response)
            #endif
            
            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                if let responseData = response.data {
                    if let model = try? decoder.decode(T.self, from: responseData) {
                        if (200...299).contains(response.response?.statusCode ?? 200) {
                            callback(.success(model))
                        } else {
                            callback(.failure(.responseError(statusCode: response.response?.statusCode ?? 401, model: model)))
                        }
                    } else {
                        callback(.failure(.parsingError))
                    }
                } else {
                    callback(.failure(.dataNotFound))
                }
            case .failure:
                callback(.failure(.apiError(statusCode: response.response?.statusCode ?? 401, error: response.error)))
            }
        }
    }
}
