//
//  ASDownloadHandler.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.
//

import Foundation

protocol DownloadDelegate: AnyObject {
    func downloadDidComplete(toLocation location: URL)
    func downloadOngoingWithProgress(progress: Double)
    func downloadDidStopWithError(error: Error?)
}

class ASDownloadHandler: NSObject, URLSessionDownloadDelegate {
   
    static let shared = ASDownloadHandler()
    weak var downloadDelegate: DownloadDelegate?
    
    func downlondFromUrl(fileURL: String, delegate: DownloadDelegate){
        let sessionConfig = URLSessionConfiguration.default
        self.downloadDelegate = delegate
        let session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        let request = URLRequest(url: URL(string: fileURL)!)
        let task = session.downloadTask(with: request)
        task.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        downloadDelegate?.downloadDidStopWithError(error: error)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        downloadDelegate?.downloadOngoingWithProgress(progress: Double(totalBytesWritten)/Double(totalBytesExpectedToWrite))
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destinationURL = documentsDirectoryURL.appendingPathComponent("\(UUID().uuidString).mp4")
        do {
            try FileManager.default.moveItem(at: location, to: destinationURL)
            downloadDelegate?.downloadDidComplete(toLocation: destinationURL)
        } catch {
            downloadDelegate?.downloadDidStopWithError(error: error)
        }
    }
}
