//
//  ASRequestContructor.swift
//  TychoNetworkHandler
//
//  Copyright © 2022 Tycho Technologies. All rights reserved.
//

import Foundation
import UIKit

public typealias ResponseClosure<T:Decodable> = (_ result: Result<T, APIError<T>>) -> (Void)

public enum RequestType: String{
    case NONE
    case GET
    case POST
    case PUT
    case DELETE
    case MULTIPART_POST
    case MULTIPART_PUT
}

public class ASRequestConstructor {
    
    public let urlFilePath:String;
    public let serviceUrls:NSDictionary;
    public var baseUrl: String? = nil;
    public var debugBaseUrl: String? = nil;
    public var releaseBaseUrl: String? = nil;
    public var headers:NSDictionary;
    
    
    public func processUpdatedRequestFor<T:Decodable>(request: ASRequestModal, callback: @escaping ResponseClosure<T>) {
        var request = request
        self.processRequestInfo(requestModal: &request);
        switch request.requestType{
            case .GET:
                ASGETRequestConstructor.getResponseForRequest(request: request) { result in
                    callback(result)
                }
            case .POST:
                ASPOSTRequestConstructor.getResponseForRequest(request: request) { result in
                    callback(result)
                }
            case .MULTIPART_POST:
                ASPOSTRequestConstructor.getResponseForMultipartRequest(request: request) { result in
                    callback(result)
                }
            case .DELETE:
                ASDELETERequestConstructor.getResponseForRequest(request: request) { result in
                    callback(result)
                }
            case .PUT:
                ASPutRequestConstructor.getResponseForRequest(request: request) { result in
                    callback(result)
                }
            case .MULTIPART_PUT:
                ASPutRequestConstructor.getResponseForMultipartRequest(request: request) { result in
                    callback(result)
                }
            default:
                break;
        }
    }
    
    init (filePath: String){
        self.urlFilePath = filePath;
        self.serviceUrls = NSDictionary.init(contentsOfFile: filePath)!;
       
        let urlDict = self.serviceUrls.object(forKey: "BASE_URLS") as? NSDictionary;
        self.baseUrl = urlDict!["BASE_URL"] as? String;
        self.debugBaseUrl = urlDict!["DEBUG_BASE_URL"] as? String;
        self.releaseBaseUrl = urlDict!["PROD_BASE_URL"] as? String;
        
        self.headers = (self.serviceUrls.object(forKey: "REQUEST_HEADERS") as? NSDictionary)!;
    }
    
    func readUrls(){
        self.baseUrl = self.serviceUrls.object(forKey: "BASE_URL") as? String;
        self.debugBaseUrl = self.serviceUrls.object(forKey: "DEBUG_BASE_URL") as? String;
        self.releaseBaseUrl = self.serviceUrls.object(forKey: "PROD_BASE_URL") as? String;
        self.headers = (self.serviceUrls.object(forKey: "REQUEST_HEADERS") as? NSDictionary)!;
    }
    
    
    func processRequestInfo( requestModal : inout ASRequestModal){
        
        let operation = requestModal.urlIdentifier;
        let urlKey = "URLS";
        var baseUrl : String?
        var operationUrl: String?
        var requestType: RequestType?
        
        let info =  (self.serviceUrls.object(forKey: urlKey) as? NSDictionary);
        let operationInfo = info!.object(forKey: operation!);
        
        if let oInfo = operationInfo{
            baseUrl = (oInfo as AnyObject).object(forKey: "BASE_URL") as? String;
            operationUrl = (oInfo as AnyObject).object(forKey: "OPERATION_URL") as? String;
    
            let request = (oInfo as AnyObject).object(forKey: "REQUEST_TYPE") as? String;
            if(request != nil){
                requestType = RequestType(rawValue: request!);
            }
        }
        
        var headers = self.serviceUrls.object(forKey: "REQUEST_HEADERS") as? [String:String];
        var relevantUrl = debugBaseUrl;
        if requestModal.isUserLoggedIn {
            if let token = requestModal.fetchToken() {
                headers?["Authorization"] = "\(token)"
            }
        }

        // set Base Url according to the configuration
        #if DEBUG
            relevantUrl = debugBaseUrl;
        #else
            if releaseBaseUrl == nil {
                relevantUrl = debugBaseUrl
            } else {
                relevantUrl = releaseBaseUrl;
            }
        #endif
        
        if(baseUrl!.count == 0){
            if(relevantUrl!.count == 0){
                baseUrl = self.baseUrl!;
            }else{
                baseUrl = relevantUrl!;
            }
        }
        if requestModal.requestHeader != nil{
            for (key,value) in (requestModal.requestHeader)!{
                headers?.updateValue(value, forKey: key)
            }
        }
        requestModal.requestHeader = headers;
        
        if(requestModal.baseUrl == "" || requestModal.baseUrl == nil){
            requestModal.baseUrl = baseUrl! as String;
        }
        
        if(requestType != nil){
            requestModal.requestType = requestType!
        }
        
        // set operation Url
        if let isTrue = requestModal.urlParameters{
            if(isTrue.count > 0){
                
                var inputUrl = operationUrl!;
                for (key, value) in requestModal.urlParameters!{
                    let resultStr = inputUrl.replacingOccurrences(of: key, with: value)
                    inputUrl = resultStr
                }
                operationUrl = inputUrl;
            }
        }else{
            requestModal.operationUrlString = operationUrl;
        }
        requestModal.operationUrlString = operationUrl ;

    }
}
