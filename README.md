# TychoNetworkHandler

[![CI Status](https://img.shields.io/travis/anurag61090/TychoNetworkHandler.svg?style=flat)](https://travis-ci.org/anurag61090/TychoNetworkHandler)
[![Version](https://img.shields.io/cocoapods/v/TychoNetworkHandler.svg?style=flat)](https://cocoapods.org/pods/TychoNetworkHandler)
[![License](https://img.shields.io/cocoapods/l/TychoNetworkHandler.svg?style=flat)](https://cocoapods.org/pods/TychoNetworkHandler)
[![Platform](https://img.shields.io/cocoapods/p/TychoNetworkHandler.svg?style=flat)](https://cocoapods.org/pods/TychoNetworkHandler)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TychoNetworkHandler is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TychoNetworkHandler'
```

## Author

Anurag Singh, anurag.singh@tychotechnologies.in

## License

TychoNetworkHandler is available under the MIT license. See the LICENSE file for more info.


