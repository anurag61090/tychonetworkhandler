Pod::Spec.new do |s|
  s.name             = 'TychoNetworkHandler'
  s.version          = '1.0.3'
  s.summary          = 'Tycho Network Manager'
  s.description      = 'Handles all the network API calls'
  s.homepage         = "https://gitlab.com/anurag61090/tychonetworkhandler.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Anurag Singh' => 'anurag.singh@tychotechnologies.in' }
  s.source           = { :git => "https://gitlab.com/anurag61090/tychonetworkhandler.git", :tag => "1.0.3" }
  s.swift_version    = '5.0'
  s.platform     = :ios, "11.0"
  s.ios.deployment_target = '11.0'
  s.tvos.deployment_target = '11.0'
  s.source_files = 'TychoNetworkHandler/AppNetwork/*.{h,swift}', 'TychoNetworkHandler/ASNetworkModals/*.swift', 'TychoNetworkHandler/ASRequestConstructor/*.swift'
  s.frameworks = 'UIKit', 'AVFoundation', 'Foundation', 'SystemConfiguration'
  s.dependency 'Alamofire'
  s.public_header_files = 'TychoNetworkHandler/AppNetwork/*.{h}'
  s.pod_target_xcconfig = {'VALID_ARCHS[sdk=iphonesimulator*]' => 'armv7 arm64 x86_64', 'VALID_ARCHS[sdk=appletvsimulator*]' => 'armv7 arm64 x86_64'}
  
end
